# Development Ambient
Padronização do ambiente de desenvolvimento bem como as ferramentas usadas

Depois de seguir o que deve ser instalado no aquivo **Configuração do ambiente de desenvolvimento produtivo** é hora de instalar extensões importante no VSCode para deixar seu ambiente mais produtivo

## VSCode Extensões importantes
O VSCode possui muitas extensões interessante que facilitam o dia a dia.

- **Settings Sync** (Obrigatória) <br />
Extensão para sincronizar tudo o que voce instalou de extensão com sua conta no github. Sincroniza todas as extensões em máquinas diferentes e recupera tudo em caso de formatação. <br />
https://github.com/shanalikhan/code-settings-sync

- **Remote WSL** (Obrigatório se estiver usando Windows)<br />
Necessário para apontar o seu VSCode para a instancia WSL2 no seu sistema<br />
https://github.com/Microsoft/vscode-remote-release

- **Material Theme Icon** (Obrigatória)<br />
Extensão para colocar icones nos arquivos<br />
https://github.com/material-theme/vsc-material-theme-icons

- **GitLens** (Obrigatória)<br />
Extensão para agilizar com o git<br />
https://github.com/eamodio/vscode-gitlens

- **TODO Tree** (Obrigatória)<br />
Facilita muito para identificar os TODO espalhados pelo pŕojeto<br />
https://github.com/Gruntfuggly/todo-tree

- **Better Comments** (Obrigatório)<br />
-Cria uma padrão de comentário de acordo com a importancia<br />
https://github.com/wayou/vscode-todo-highlight

- **CodeStream: GitHub, GitLab, Bitbucket PRs and Code Review** (análise da equipe) <br />
Muito bom para comentário de código entre equipe.<br />
https://github.com/TeamCodeStream/CodeStream

- **Docker** (Obrigatório)<br />
Ajuda construir, gerenciar e implantar aplicativos em contêineres<br />
https://github.com/microsoft/vscode-docker

- **Dracula Official** (Thema sugerido, mas escolha outro que te agrade)<br />
Theme muito utilizado pelos desenvolvedores<br />
https://github.com/dracula/visual-studio-code

- **Path Intellisense** (Opcional, mas muito boa)<br />
Ajuda no auto completar durante o código<br />
https://github.com/ChristianKohler/PathIntellisense

- **Prettier** - Code formatter (Opcional, mas interessante)<br />
Ajuda na visualização do seu código<br />
https://github.com/prettier/prettier-vscode

- **Git History** - (Opcional)<br />
Ajuda na comparação das branches, e pode ser um complemento para seu trabalho<br />
https://github.com/DonJayamanne/gitHistoryVSCode

## Existem muitas outras extensões que podem ajudar, mas a intensão é criar um padrão para a equipe de desenvolvimento com objetivo de trazer produtividade coletiva.



